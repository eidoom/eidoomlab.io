import calendar
import urllib.parse


class Filters:
    def fmonth(n):
        return calendar.month_name[n][:3]

    def archiveurl(archiveprefix):
        match archiveprefix:
            case "arxiv":
                return "https://arxiv.org/abs/"
            case _:
                print(f"Warning: unrecognised archiveprefix {archiveprefix}")

    def repoprefix(repourl):
        return urllib.parse.urlparse(repourl).netloc.split(".")[0]

    def repopath(repourl):
        url = urllib.parse.urlparse(repourl)
        path = url.path[1:]
        return path.split("/")[1] if url.netloc == "zenodo.org" else path

    def fdate(d):
        return d.strftime("%d %b %Y")

    def flagcode(country):
        return {
            "England": "gb-eng",
            "Germany": "de",
            "Italy": "it",
            "Scotland": "gb-sct",
            "South Korea": "kr",
            "Switzerland": "ch",
            "Wales": "gb-wls",
        }[country]

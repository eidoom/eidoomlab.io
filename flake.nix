{
  description = "A Python with Jinja development environment for Linux";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-24.11";
  };

  outputs = { nixpkgs, ... }: let
    system = "x86_64-linux";
    pkgs = import nixpkgs { inherit system; };
  in {
    devShells."${system}".default = pkgs.mkShell {
      packages = [
        (pkgs.python3.withPackages (python-pkgs: with python-pkgs; [
          jinja2
        ]))
      ];
    };
  };
}

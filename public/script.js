function findDetails(node) {
  return node === null || node.tagName === "DETAILS"
    ? node
    : findDetails(node.parentNode);
}

function openTarget() {
  const hash = window.location.hash.substring(1);
  const target = document.getElementById(hash);
  const details = findDetails(target);
  if (details) {
    details.open = true;
  }
}

window.addEventListener("hashchange", openTarget);

openTarget();

# [eidoom.gitlab.io](https://gitlab.com/eidoom/eidoom.gitlab.io)

[Live](https://eidoom.gitlab.io/)

## Build

```shell
pdm install
pdm run ./build.py
python -m http.server -d public
```

Alternatively, there is experimental Nix Flake support.

## TODO

- move url prefixes from data fields to template
- link between proceeding contribution and relevant talk, and other cross-referencing hyperlinks where appropriate
- general styling, mainly spacing, maybe title sizing, maybe add feature colour
- have timeline down left side
- make sure CI works
- use time elements for paper/talk dates
- add counter beside closed accordions
- can I make use of the side margins?
- put more content on there
- code bases should go in their own lists
- put dissertation and summer research project abstracts in accordions
- flag font
- use image-set() for background-image to offer alternative sizes and formats
- horizontal margins are too big on mobile
- do CSS with single grid for St A Courses

## Attribution

Flags from <https://flagicons.lipis.dev/>.

#!/usr/bin/env python3

import datetime
import json
import pathlib

import jinja2

import filters

if __name__ == "__main__":
    env = jinja2.Environment(
        loader=jinja2.FileSystemLoader("templates"),
        trim_blocks=True,
        lstrip_blocks=True,
    )

    env.filters |= {
        k: v for k, v in filters.Filters.__dict__.items() if not k.startswith("__")
    }

    template = env.get_template("index.html.jinja")

    data = {}

    datadir = pathlib.Path("data")

    for x in ("pubs", "pres", "prizes", "courses", "sps"):
        with open(datadir / f"{x}.json") as f:
            data[x] = json.load(f)

    data["pres"] = [
        {"date": datetime.date(p["year"], p["month"], p["day"]), **p}
        for p in data["pres"]
    ]

    data["today"] = datetime.date.today()

    html = template.render(data)

    with open("public/index.html", "w") as f:
        f.write(html)
